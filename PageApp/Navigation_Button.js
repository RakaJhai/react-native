import React from 'react'
import {View, Text, Button, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import NewsScreen from './src/components/NewsComponent';
import HomeScreen from './src/screens/DashboardScreens';

const Stack = createStackNavigator();

function App(){
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} options={{title: ''}}/>
        <Stack.Screen name="News" component={NewsScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;

