import React from 'react';
import {View, Button, StyleSheet} from 'react-native';

export default function HomeScreen({navigation}){
    return(
      <View style={styles.button}>
        <Button
          title= "News"
          onPress={()=>navigation.navigate('News')}
        />
      </View>    
    )
}
const styles = StyleSheet.create({
    button:{
        display: 'flex',
        flex: 1,
        justifyContent:'center',
        fontSize: 36,
        fontWeight: '900',
    }
})