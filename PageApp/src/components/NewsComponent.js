import React from 'react';
import axios from 'axios';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class NewsScreen extends React.Component{
    constructor(props){
        super(props);
        this.state = {
          listNews: [],
        }
    }
  
    componentDidMount(){ //otomatis terpanggil dan hanya sekali
      axios.get('http://newsapi.org/v2/everything?q=bitcoin&from=2020-09-22&sortBy=publishedAt&apiKey=e81733d2d73b445cbd47e4f5581a1ce0')
      .then(res=>{
        this.setState({listNews: res.data.articles})
      }).catch(err=>{

      })
    }
    render(){
      return(
        <ScrollView>
          <Text style={styles.header}>NEWS FOR YOU</Text>
          <View style={styles.container}>
              {this.state.listNews.map((news, index) => (
                <View style={styles.item} key={index}>
                  <View style={styles.imageContent}>
                    <Image source={{uri: news.urlToImage}} style={styles.image}></Image>
                  </View>
                  <View style={styles.content}>
                    <Text style={styles.title}>{news.title}</Text>
                    <Text style={styles.author}>{news.author} {news.publishedAt}</Text>
                    <Text style={styles.desc}>{news.description}</Text>
                  </View>
                </View>
              ))}
          </View>
        </ScrollView>
      )
    }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#000',
    color: '#fff',
    fontSize: 30,
    fontWeight: '900',
    padding: 10,
    textAlign: 'center',
    marginBottom: 5
  },
  item:{
    display: "flex",
    flexDirection: "row",
    backgroundColor: '#ffffff',
    paddingVertical: 5,
    borderBottomWidth: 3,
    borderBottomColor: '#737373',
    // borderBottomWidth: StyleSheet.hairlineWidth,
  },
  imageContent:{
    flex: 1,
    paddingRight: 5,
    justifyContent: 'center'
  },
  image:{
    width: 'auto',
    height: 100
  },
  content:{
    flex: 3,
    justifyContent: "center"
  },
  title:{
    fontWeight: "bold",
    fontSize: 16,
  },
  author:{

  }
})