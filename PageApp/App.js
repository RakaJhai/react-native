import 'react-native-gesture-handler';
import React from 'react'
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

function HomeScreen(){
  return(
    <View>
      <Text>Ini adalah Home Screen App.js</Text>
    </View>
  )
}

function App(){
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          // options = {{'Overview'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;