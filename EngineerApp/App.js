import 'react-native-gesture-handler';
import React from 'react';
import AppStack from './src/navigator.js/AppStack';
import {Provider} from 'react-redux';
import store from './src/store';

const App = () => (
    <Provider store={store}>
        <AppStack />
    </Provider>
);

export default App;