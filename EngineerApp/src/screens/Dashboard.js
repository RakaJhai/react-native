import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import WeatherComponent from '../components/WeatherComponent';
import PromotionComponent from '../components/PromotionComponent';
import {logoutAction} from '../redux/action/authAction';
import { connect } from 'react-redux';

function Dashboard(props){
    return(
        <View>
            
            <PromotionComponent />
            <WeatherComponent style={styles.container}/>

            <Button color= '#000000' onPress={()=> props.navigation.navigate('Jokes')} title="Go to Joke"></Button>
            <Button color= '#000000' onPress={()=> props.processLogout()} title="LOGOUT"></Button>
            
            <Text style={{fontSize: 24}}>Username : {props.auth.userName}</Text>
        </View> 
    )
}

const mapStateTopProps = (state) => ({
    auth: state.auth1,
});

const mapDispatchToProps = (dispatch) => ({
    processLogout: () => dispatch(logoutAction())
});

export default connect(mapStateTopProps, mapDispatchToProps) (Dashboard)

const styles = StyleSheet.create({
    container:{
        borderRadius: 25,
        borderWidth: 10
    }
})