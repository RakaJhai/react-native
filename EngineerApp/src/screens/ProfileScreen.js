import React from 'react';
import { Text, View, Image } from 'react-native';
import { connect } from 'react-redux';

function ProfileScreen(props){
    return (
        <View>
            <Text>Ini adalah ProfileScreen</Text>
            <Text>Username: {props.username}</Text>
            <Text>First Name: {props.first_name}</Text>
            <Text>Last Name: {props.last_name}</Text>
            <Image  source={{uri: props.profile_url}} style={{width: 100, height: 100}}/>
        </View>
    )
}

const mapStateTopProps = (state) => ({
    username: state.profile.username,
    first_name: state.profile.first_name,
    last_name: state.profile.last_name,
    profile_url: state.profile.photo_url
  });
  
  const mapDispatchToProps = (dispatch) => ({
    // processLogin: (data) => 
    //   dispatch(loginAction(data))
  });
  
  export default connect(mapStateTopProps, mapDispatchToProps) (ProfileScreen)
  