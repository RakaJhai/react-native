import React from 'react';
import {View, Text, Button, TextInput} from 'react-native';
import { connect } from 'react-redux';
import { addTodo, removeTodo } from '../redux/action/todoAction';

const mapStateTopProps = (state) => ({
    todos: state.todos,
});

const mapDispatchToProps = (dispatch) => ({
    addTodo: (text) => dispatch(addTodo(text)),
    delete: (id) => dispatch(removeTodo(id))
});

class TodoScreen extends React.Component{
    constructor(props){
        super(props);
        this.state={
            newTodo:'',
        }
    }
    render(){
      
        return(
            <View>
                <Text style={{alignItems: 'center'}}> TOD LIST </Text>
                {this.props.todos.map((item, index) =>(
                    <View key={index}>
                        <Text>
                            {item.id + 1}. {item.text} --{' '}
                            {item.completed ? 'DONE' : 'IN PROGERSS'}
                        </Text>
                        <Button onPress={() => this.props.delete(item.id)} title="Remove"></Button>
                    </View>
                ))}

                <View>
                    <TextInput 
                        placeholder="Typee new task here"
                        onChangeText={(text) => this.setState({newTodo: text})}
                        style={{backgroundColor: '#ffffff', padding:10, marginBottom: 10}}
                    />
                    <Button
                        onPress={() => this.props.addTodo(this.state.newTodo)} title="ADD TO DO"
                    />

                </View>
            </View>
        )
    }
}

export default connect(mapStateTopProps, mapDispatchToProps) (TodoScreen)