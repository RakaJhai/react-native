import React, {useEffect} from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import {getSkillsDetail} from '../redux/action/skills';

function UpdgradeSkills(props){
    useEffect(() => {
        props.ambilData();
    }, [])

    const loopData = props.interest.map((item, index) =>{
        return(
            <Text key={index} style={styles.skillsList}>{item}</Text>
        )
    })
    const loopContent = props.interestContent.map((item, index) => {
        return (
            <View style= {styles.contentItem} key={index}>
                <Text style= {styles.title}>{item.title}</Text>
                <Image style= {styles.image} source={{uri: item.preview_url}} />
                <Text style= {styles.description}>{item.description}</Text>
            </View>
        )
    })
    return (
        <ScrollView>
            <View style= {styles.container}>
                {loopData}
                {loopContent}
            </View>
        </ScrollView>
    )
}

const mapStateToProps = (state) => ({
    interest: state.skills.interests,
    interestContent: state.contentReducer
  });
  
  const mapDispatchToProps = (dispatch) => ({
    ambilData: () => dispatch(getSkillsDetail())
  });
  
  export default connect(mapStateToProps, mapDispatchToProps) (UpdgradeSkills)
  
  const styles = StyleSheet.create({
      container:{
        marginHorizontal: 5,
        justifyContent: 'center'
      },
      contentItem:{
          marginVertical: 10
      },
        skillsList:{
            display: 'flex',
            flexDirection: 'row',
            flex: 4,
            width: 150,
            fontSize: 18,
            borderRadius: 25,
            backgroundColor: '#000',
            color: '#fff',
            padding: 10,
            marginVertical: 5
        },
        title:{
            fontSize: 20,
            fontWeight: '700',
        },
        image:{
            width: '100%',
            height: 200,
            marginHorizontal: 5
        }

  })