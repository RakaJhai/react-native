// import { StackActions } from '@react-navigation/native';
import React from 'react';
import {Text, TextInput, View, StyleSheet, TouchableOpacity, ActivityIndicator} from 'react-native';
import {loginAction} from '../redux/action/authAction';
import { connect } from 'react-redux';
// import auth from '../redux/reducer/auth';
class LoginPage extends React.Component{
    constructor(props){
        super(props);
        this.state={
            userName: "",
            password: "",
            message: ""
        }
    }
    
    render(){
      const login = () =>{
        if(!this.state.userName){
          this.setState({message : 'Username Wajib diisi'});
        }else if(!this.state.password){
          this.setState({password: 'Password wajib diisi'});
        }else{
          this.props.processLogin({username: this.state.userName, password: this.state.password});
        }
      }
        return(
            <View style={styles.container}>
                <Text style= {styles.logo}>Engineer App</Text>
                
                <View style={styles.inputView}>
                    <TextInput 
                        style={styles.inputText}
                        placeholder="Enter username ..."
                        placeholderTextColor="#003f5c"
                        value={this.state.userName}
                        onChangeText={text => this.setState({userName: text})}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput 
                        style={styles.inputText}
                        placeholder="Enter password ..."
                        placeholderTextColor="#003f5c"
                        value={this.state.password}
                        onChangeText={text=> this.setState({password: text})}
                        secureTextEntry
                    />
                </View>
                {this.props.isLoading ? (
                  <TouchableOpacity
                    onPress={() => console.info('disabled')}
                    style={{borderRadius: 50, backgroundColor: '#f2f2f2', padding: 15}}>
                    <ActivityIndicator size="large" color="#333333" />
                  </TouchableOpacity>
                ) : (
                  <>
                    <TouchableOpacity 
                      style={styles.loginBtn} 
                      onPress={()=> login()} title="LOGIN">
                      <Text style={styles.loginText}>Login</Text>
                    </TouchableOpacity>
                  </>
                )}

                <TouchableOpacity 
                  style={styles.registerBtn} 
                  onPress={()=> this.props.navigation.navigate('Register')} title="REGISTER">
                  <Text style={styles.loginText}>REGISTER</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const mapStateTopProps = (state) => ({
  isLoading: state.auth.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  processLogin: (data) => 
    dispatch(loginAction(data))
});

export default connect(mapStateTopProps, mapDispatchToProps) (LoginPage)

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#4f4f4f',
      alignItems: 'center',
      justifyContent: 'center',
      alignItems: ''
    },
    logo:{
      fontWeight:"bold",
      fontSize:50,
      color:"#fb5b5a",
      marginBottom:40
    },
    inputView:{
      width:"80%",
      backgroundColor:"#fff",
      borderRadius:25,
      height:50,
      marginBottom:20,
      justifyContent:"center",
      padding:20
    },
    inputText:{
      height:50,
      color:"#000000"
    },
    forgot:{
      color:"white",
      fontSize:11
    },
    loginBtn:{
      width:"80%",
      backgroundColor:"#fb5b5a",
      borderRadius:25,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:40,
      marginBottom:10
    },
    registerBtn:{
      width:"80%",
      backgroundColor:"#642EFE",
      borderRadius:25,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:10
    },
    loginText:{
      color:"white"
    }
  });