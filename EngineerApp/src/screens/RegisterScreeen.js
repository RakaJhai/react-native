import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { connect } from 'react-redux';

function RegisterSCreen(props){
    const[username, setUsername] = useState(null);
    const[password, setPassword] = useState(null);
    const[firstName, setFirstName] = useState(null);
    const[lastName, setLastName] = useState(null);
    const[message, setMessage] = useState(null);
    
    //untuk validasi input
    const checkRegister = () =>{
        if(!username){
            setMessage('Username is required')
        }else if(!password){
            setMessage('Password is required')
        }else{
            setMessage(null);
            const dataRegister={
                username: username,
                password: password,
                first_name: firstName,
                last_name: lastName
            }
            props.processRegister(dataRegister);
        }
    }

    return(
        <View style={styles.container}>
            <View style={styles.inputView}>
                <TextInput style={styles.inputText} placeholder="Enter username" onChangeText = {(text) => setUsername(text)} value={username}></TextInput>
            </View>
            <View style={styles.inputView} >
                <TextInput style={styles.inputText} placeholder="Enter password" onChangeText = {(text) => setPassword(text)} value={password}></TextInput>
            </View>
            <View style={styles.inputView} >
                <TextInput style={styles.inputText} placeholder="Enter firstname" onChangeText = {(text) => setFirstName(text)} value={firstName}></TextInput>
            </View>
            <View style={styles.inputView}>
                <TextInput style={styles.inputText} placeholder="Enter lastname" onChangeText = {(text) => setLastName(text)} value={lastName}></TextInput>
            </View>
            <Text style={{color:'red'}}>{message}</Text>
            <TouchableOpacity 
                  style={styles.registerBtn} 
                  onPress={()=> checkRegister()} title="REGISTER">
                  <Text style={styles.loginText}>REGISTER</Text>
            </TouchableOpacity>
        </View>
    )
}


const mapStateToState = (state) =>({

})

const mapDispatchToProps = (dispatch) =>({
    processRegister: (data) => dispatch({type: 'REGISTER', payload: data})
})

export default connect(mapStateToState, mapDispatchToProps) (RegisterSCreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#4f4f4f',
        alignItems: 'center',
        justifyContent: 'center',
      },
      registerBtn:{
        width:"80%",
        backgroundColor:"#fb5b5a",
        borderRadius:25,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:40,
        marginBottom:10
      },
      inputView:{
        width:"80%",
        backgroundColor:"#fff",
        borderRadius:25,
        height:50,
        marginBottom:20,
        justifyContent:"center",
        padding:20
      },
      inputText:{
        height:50,
        color:"#000000"
      },
})
