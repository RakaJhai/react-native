import axios from 'axios';
import { ToastAndroid } from 'react-native';
import {put, takeLatest} from 'redux-saga/effects';
import {LOGIN} from '../action/all_types'
function* register(action){
    try{
        const resRegister = yield axios({
            method: 'POST',
            url: 'http://192.168.0.12:3000/auth/register',
            data: action.payload
        })

        if(resRegister && resRegister.data){
            //save data to store(reducer)
            yield put({type: 'REGISTER_SUCCERSS', username: resRegister.data.username});
            //show message berhasil
            ToastAndroid.showWithGravity(
                'Register successfully',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM
            )

            yield put({type: LOGIN})


        }else{
            ToastAndroid.showWithGravity(
                'Register unsuccessfully',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM
            )
        }
    }catch(err){

    } 
}

function* authRegisterSaga(){
    yield takeLatest('REGISTER', register);
}

export default authRegisterSaga;