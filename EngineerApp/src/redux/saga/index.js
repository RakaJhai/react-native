import {all} from 'redux-saga/effects';
import authSaga from './auth';
import interestContentSaga from './interestContentSaga';
import profileSaga from './profile';
import skillSaga from './skillsSaga';
import authRegisterSaga from './authRegisterSaga';
export default function* rootSaga() {
  yield all([authSaga(), profileSaga(), skillSaga(), interestContentSaga(), authRegisterSaga()]);
}
