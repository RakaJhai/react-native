import {takeLatest, put} from 'redux-saga/effects';
import {apiInterestContent} from '../../common/api/interestContent';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_CONTENT,
  GET_CONTENT_SUCCESS,
  GET_CONTENT_FAILED,
} from '../action/all_types';

function* getContentDetail(action){
    try{    
        const headers = yield getHeaders();
        const accountId = yield getAccountId();

        //FETCH SKILLS DATA
        // console.log("halo", action.payload)
        const resSkills =  yield apiInterestContent(action.payload, headers);
        yield put({type: GET_CONTENT_SUCCESS, payload: resSkills.data});
        // console.log("LOhaa", resSkills.data)
    }
    catch(e){
        //show alert
        ToastAndroid.showWithGravity(
            'Gagal mengambil data InterestContent',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
        );
        yield put({type: GET_CONTENT_FAILED});
    }
}

function* interestContentSaga(){
    yield takeLatest(GET_CONTENT, getContentDetail)
}

export default interestContentSaga;