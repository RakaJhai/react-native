import {takeLatest, put} from 'redux-saga/effects';
import {apiGetInterestDetail} from '../../common/api/skills';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_SKILLS,
  GET_SKILLS_SUCCESS,
  GET_SKILLS_FAILED,
} from '../action/all_types';
import { GET_CONTENT } from '../action/all_types';

function* getSkillsDetail(){
    try{    
        const headers = yield getHeaders();
        const accountId = yield getAccountId();

        //FETCH SKILLS DATA
        const resSkills =  yield apiGetInterestDetail(accountId, headers);
        yield put({type: GET_SKILLS_SUCCESS, payload: resSkills.data});

        yield put({type: GET_CONTENT, payload: resSkills.data.interests[0]})
        console.log("SKILLSS SAGA!!!")  
    }
    catch(e){
        //show alert
        ToastAndroid.showWithGravity(
            'Gagal mengambil data skills',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
        );
        yield put({type: GET_SKILLS_FAILED});
    }
}

function* skillSaga(){
    yield takeLatest(GET_SKILLS, getSkillsDetail)
}

export default skillSaga;