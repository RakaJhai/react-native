import {GET_SKILLS, GET_SKILLS_SUCCESS, GET_SKILLS_FAILED} from '../action/all_types';
import {LOGOUT} from '../action/all_types';

const initialState = {
    isLoading: false,
    user_id: null,
    interests: [],
}

const skills = (state = initialState, action) =>{
    switch(action.type){
        case GET_SKILLS:{
            return{
                ...state,
                isLoading: false,
            }
        }
        case GET_SKILLS_SUCCESS:{
            return{
                ...state,
                ...action.payload,
                isLoading: false,
            }
        }
        case GET_SKILLS_FAILED:{
            return{
                ...state,
                isLoading: true,
            }
        }
        case LOGOUT:{
            return{
                ...state,
                isLoading: false,
                id: null,
                interest: null,
            }
        }
        default:
            return state
    }
}

export default skills;