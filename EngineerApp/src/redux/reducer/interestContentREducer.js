import {GET_CONTENT_SUCCESS} from '../action/all_types';
import {LOGOUT} from '../action/all_types';

const initialState = []

const contentReducer = (state = initialState, action) =>{
    switch(action.type){
        case GET_CONTENT_SUCCESS:{
            return action.payload
                // isLoading: false,
        }
        case LOGOUT:{
            return[]
        }
        default:
            return state
    }
}

export default contentReducer;