const initialState = {
    userName: null,
    isLoggedIn: false
};

const todos = (state = initialState, action) =>{
    switch(action.type){
        case 'LOGIN':{
            return{
                ...state,
                userName: action.userName,
                isLoggedIn: true,
            }
        }
        case 'LOGOUT':{
            return{
                ...state,
                userName: null,
                isLoggedIn: false
            }
        }
        default:
            return state;
    }
};

export default todos;