import {LOGIN, LOGIN_SUCCESS, LOGIN_FAILED, LOGOUT} from '../action/all_types';

const initialState = {
    isLoading : false,
    isLoggedIn: false,
    username: null
}

const auth = (state = initialState, action) =>{
    switch(action.type){
        case LOGIN: {
            return{
                ...state,
                isLoading: true
            };
        }
        case LOGIN_SUCCESS:{
            return{
                ...state,
                isLoggedIn: true,
                isLoading: false,
            }
        }
        case LOGIN_FAILED:{
            return{
                ...state,
                isLoading: false,
            }
        }
        case LOGOUT:{
            return{
                ...state,
                isLoggedIn: false,
                isLoading: false,
            }
        }
        case 'REGISTER':{
            return{
                ...state,
                isLoading: true
            }
        }
        case 'REGISTER_SUCCESS':{
            return{
                ...state,
                username: action.username, //menyimpan data username dari register saga
                isLoading: false
            }
        }
        default :
            return state;
    }
}

export default auth;