import {combineReducers} from 'redux';
import todos from './todoReducer';
import auth from './authReducer';
import auth1 from './auth';
import profile from './profile';
import skills from './skills';
import contentReducer from './interestContentREducer';

export default combineReducers({
    todos,
    auth,
    auth1,
    profile,
    skills,
    contentReducer,
});