import {GET_CONTENT} from './all_types';

export const getContentDetailAction = () => {
  return {type: GET_CONTENT};
};