import {GET_PROFILE} from './all_types';

export const getProfileDetail = () => {
  return {type: GET_PROFILE};
};
