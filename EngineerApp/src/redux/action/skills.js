import {GET_SKILLS} from './all_types';

export const getSkillsDetail = () => {
  return {type: GET_SKILLS};
};