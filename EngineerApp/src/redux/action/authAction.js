import {LOGIN, LOGOUT} from './all_types';
export const loginAction = (payload) => {
    return {type: LOGIN, payload}
}

export const logoutAction = () =>{
    return {type: LOGOUT}
}