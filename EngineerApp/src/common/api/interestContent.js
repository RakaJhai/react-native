import axios from 'axios';

export function apiInterestContent(dataInterestContent, headers) {
  return axios({
    method: 'GET',
    url: 'http://192.168.0.12:3000/content/'+ dataInterestContent,
    headers,
  });
}