import axios from 'axios';

export function apiFetchProfileDetail(id, headers) {
  return axios({
    method: 'GET',
    url: 'http://192.168.0.12:3000/profile/' + id,
    headers,
  });
}
