import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigator from './MainNavigator';
import LoginPage from '../screens/LoginPage';

import { connect } from 'react-redux';
import RegisterSCreen from '../screens/RegisterScreeen';

const Stack = createStackNavigator();

function AppStack(props){
    return(
        <NavigationContainer>
            <Stack.Navigator >
                {props.sudahLogin ? (
                    <Stack.Screen name="Navigator" component={MainNavigator} /> 
                ):(
                    <>
                        <Stack.Screen name="Login" component={LoginPage}/>
                        <Stack.Screen name="Register" component={RegisterSCreen}/>
                    </>
                )}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const mapStateTopProps = (state) => ({
    sudahLogin: state.auth1.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateTopProps, mapDispatchToProps) (AppStack)
