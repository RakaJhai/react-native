import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Dashboard from '../screens/Dashboard'
import JokesComponent from '../components/JokesComponent';

const StackDash = createStackNavigator();
export default function StackDashScreen(){
  return(
    <StackDash.Navigator>
      <StackDash.Screen name="Dashboard" component={Dashboard}></StackDash.Screen>
      <StackDash.Screen name="Jokes" component={JokesComponent}></StackDash.Screen>
    </StackDash.Navigator>
  )
}