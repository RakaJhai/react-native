 
 import 'react-native-gesture-handler';
 import React from 'react';
 
//  import WeatherComponent from '../components/WeatherComponent';
//  import PromotionsComponent from '../components/PromotionComponent';
//  import TodoScreen from '../screens/TodoScreen';
 import StackDashScreen from './DashboardStack';
 import ProfileScreen from '../screens/ProfileScreen';
 import UpgradeSkkills from '../screens/UpgradeSkillScreen';
 import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import InterestContent from '../screens/InterestContent';
import Ionicons from 'react-native-vector-icons/Ionicons';

 const Tab = createBottomTabNavigator();
 export default function MainNavigator(){
    return(
        <Tab.Navigator
            screenOptions={({route}) => ({ 
                tabBarIcon: ({focused, color, size}) => { 
                    let iconName; 
                    if (route.name === 'All') 
                        {
                            iconName = focused ? 'home-outline' : 'home';
                            return <Ionicons name={iconName} size={size} color={color} />;
                        } 
                    else if (route.name === 'Profile') 
                        { 
                            iconName = focused ? 'newspaper-outline' : 'newspaper';
                            return <Ionicons name={iconName} size={size} color={color} />;
                        } 
                    else if (route.name === 'Skilss') 
                            { iconName = focused ? 'person-outline' : 'person'; }
                    return <Ionicons name={iconName} size={size} color={color} />;
                }
        
            })}
        >
            <Tab.Screen name="All" component={StackDashScreen}/>
            {/* <Tab.Screen name="Promotions" component={PromotionsComponent} />
            <Tab.Screen name="Weather" component={WeatherComponent}/> */}
            {/* <Tab.Screen name="TodoScreen" component={TodoScreen} /> */}
            <Tab.Screen name="Profile" component={ProfileScreen} />
            <Tab.Screen name="Skilss" component={UpgradeSkkills} />
            {/* <Tab.Screen name="Content" component={InterestContent} /> */}
        </Tab.Navigator>
    )
 }