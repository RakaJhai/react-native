import React from 'react';
import {Text,View,SafeAreaView, Image,StyleSheet, Dimensions} from 'react-native';
import axios from'axios';
import Carousel from 'react-native-snap-carousel';

export default class PromotionsComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          activeIndex:0,
          listImage: [],
      }
    }

    componentDidMount() {
      this.getPromData();
    // call axios
    // axios.get('http://localhost:5000/promotions')
    //   .then((res) => {
    //     console.info('Data :', res.data[1].preview_image);
    //     this.setState({
    //       listImage: res.data,
    //     });
    //   })
    //   .catch((err) => {
    //     console.log("Error Get Promotions");
    //   });
    
    }

    async getPromData(){
      try{
        let res = await axios.get('http://localhost:5000/promotions');
        this.setState({
          listImage: res.data,
        });
      }
      catch(err){
        console.log(err, "EROOR PROMOTION!!")
      }
    }

    _renderItem({item,index}){
        return (
          <View style={{
      		backgrounColor:'#fff',
      		// borderRadius:5,
          	height: 150,
          	marginBottom:20,
            shadowColor: "#000",
			shadowOffset: {
				width: 0,
				height: 12,
			},
			shadowOpacity: 0.58,
			shadowRadius: 16.00,
			elevation: 24,
              }}>
            <Image source = {{uri: item.preview_image}} style={styles.carousel}/>
        	{/* <Text>sadasdsadasdas</Text> */}
          </View>

        )
    }
    
    render() {
        return (
         	<View>
                <Carousel
                  layout={"default"}
                  ref={ref => this.carousel = ref}
                  data={this.state.listImage}
                  sliderWidth={Dimensions.get('window').width}
                  itemWidth={Dimensions.get('window').width}
                  renderItem={this._renderItem}
                  onSnapToItem = { index => this.setState({activeIndex:index}) } />
         	</View>
        );
    }
}

const styles = StyleSheet.create({
  carousel : {
    width:'100%',
    height:'100%',
    resizeMode:'cover',
    // borderRadius:15,
   
  }
});