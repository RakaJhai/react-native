import axios from 'axios';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class JokesData extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listJokes:[],
            selected_jokes:{}
        }
    }
    
    componentDidMount(){
        this.getData();
    }

    async getData(){
        // axios.get('http://localhost:5000/jokes')
        // .then(res=>{
        //     const jokes_data = res.data;
        //     this.setState({listJokes: jokes_data})
        //     this.interval = setInterval(()=>{
        //         const randomData = Math.floor(Math.random()*this.state.listJokes.length);
        //         this.setState({selected_jokes: this.state.listJokes[randomData]})
        //     }, 5000)
        // }).catch(err =>{
        //     console.log("Error Get Jokes!!!")
        // })
        try{
            let res = await axios.get('http://localhost:5000/jokes')
            const jokes_data = res.data;
            this.setState({listJokes: jokes_data})
            this.interval = setInterval(()=>{
                const randomData = Math.floor(Math.random()*this.state.listJokes.length);
                this.setState({selected_jokes: this.state.listJokes[randomData]})
            }, 5000)
        }
        catch(err){
            console.log(err, 'ERROR GUYS!!')
        }

    }

    componentWillUnmount(){
        this.interval && clearInterval(this.interval)
    }
  
    render(){
        return(
            <View style={styles.jokesitem}>
              <Text>{this.state.selected_jokes.jokes}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    jokesitem:{
        height: 100,
        fontSize: 26,
        fontWeight: '400',
        // borderWidth: 2,
        borderRadius: 20,
        padding: 15,
        marginBottom: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#d3d3d3'
    }
})