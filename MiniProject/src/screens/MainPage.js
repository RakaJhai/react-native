import React from 'react';
import { View } from 'react-native';
import MainNavigator from '../navigation/MainNavigation';
import { NavigationContainer } from '@react-navigation/native';

function MainPage(){
    return(
        <MainNavigator />
    )
}

export default MainPage;