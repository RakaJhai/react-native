import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Profile from '../components/Profile';
import Home from '../components/Home';
import Review from '../components/Review';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Image} from 'react-native';
const Tab = createBottomTabNavigator();

function MainNavigation(){
    return(
            <Tab.Navigator 
                screenOptions={({route}) => ({ 
                    tabBarIcon: ({focused, color, size}) => { 
                        let iconName;
                        let iconProfile;
                        if (route.name === 'Review'){
                            iconName = focused ? 'chatbox-ellipses-outline' : 'chatbox';
                            return <Ionicons name={iconName} size={size} color={color} />;
                        } 
                        else if (route.name === 'Home'){ 
                            iconName = focused ? 'home-sharp' : 'home';
                            return <Ionicons name={iconName} size={size} color={color} />;
                        }
                        else if (route.name === 'Profile') {
                            iconProfile = require('../../assets/images/profile.webp')
                            return <Image style={{width: 30, height: 30}} source={iconProfile}/>
                        }
                    }
                })}
                tabBarOptions={{
                    activeTintColor: '#fff',
                    inactiveTintColor: 'gray',
                  }}
                tabBarOptions={{showLabel: false}}
            >
                <Tab.Screen name="Review" component={Review} screenOptions={{title:"Test"}}></Tab.Screen>
                <Tab.Screen name="Home" component={Home}/>
                <Tab.Screen name="Profile" component={Profile}/>
            </Tab.Navigator>
        
    )
}

export default MainNavigation;