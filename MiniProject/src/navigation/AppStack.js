import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginComponent from '../components/LoginComponent';
import SignUp from '../components/SignUp';
import MainPage from '../screens/MainPage';

const Stack = createStackNavigator();

function AppStack(){
    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown:false}}>
                <Stack.Screen name="Login" component={LoginComponent}/>
                <Stack.Screen name="SignUp" component={SignUp}/>
                <Stack.Screen name="Review" component={MainPage}/>
                <Stack.Screen name="Logout" component={LoginComponent}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppStack;