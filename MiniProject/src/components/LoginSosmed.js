import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet } from 'react-native';

function LoginSosmed(){
    return(
        <View>
            <View style={styles.container}>
                <TouchableOpacity>
                    {/* <Text style={{color:'#fff'}}>Social Media Login</Text> */}
                    <Image style={styles.sosmedIcon} source={require('../../assets/images/facebook.png')}></Image>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image style={styles.sosmedIcon} source={require('../../assets/images/googleplus.png')}></Image>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image style={styles.sosmedIcon} source={require('../../assets/images/linkedin.png')}></Image>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default LoginSosmed;

const styles = StyleSheet.create({
    container:{
        display: "flex",
        flexDirection: "row",
        // marginVertical: 20
    },
    sosmedIcon:{
        width: 50,
        height: 50,
        marginTop: 20,
        marginHorizontal: 20
    },
})