import React from 'react';
import { View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

function Profile(props){
    return(
        <View>
            <TouchableOpacity
                onPress = {()=> props.navigation.navigate('Logout')}
            >
                <Text>LOGOUT</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Profile;