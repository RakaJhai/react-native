import React, {useState} from 'react';
import { View, Image, TextInput, TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import LoginSosmed from './LoginSosmed'; 

const consWidth = Dimensions.get('window').width * 0.8;
const consHeigth = Dimensions.get('window').height;

function SignUp(props){
    const[name, setName] = useState(null);
    const[email, setEmail] = useState(null);
    const[userName, setUsername] = useState(null);
    const[password, setPassword] = useState(null);
    const[message, setMessage] = useState(null);

    const login=()=>{
        if(!name){
            setMessage('name is required')
        }else if(!email){
            setMessage('email is required')
        }else if(!userName){
            setMessage('userName is required')
        }else if(!password){
            setMessage('password is required')
        }
        else{
            return state;
        }
    }
    return(
        // <View>
            <View style={styles.container}>
                    <View style={styles.profilePicture}>
                        <Image style={styles.profileImg} source={require("../../assets/images/profile.webp")}></Image>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Name"
                            placeholderTextColor= "#fff"
                            onChangeText = {(text) => setName(text)}
                            value={name}
                        ></TextInput>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Username"
                            placeholderTextColor= "#fff"
                            onChangeText = {(text) => setEmail(text)}
                            value={email}
                        ></TextInput>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Email"
                            placeholderTextColor= "#fff"
                            onChangeText = {(text) => setUsername(text)}
                            value={userName}
                        ></TextInput>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Password"
                            placeholderTextColor= "#fff"
                            onChangeText = {(text) => setPassword(text)}
                            value={password}
                        ></TextInput>
                    </View>
                    <TouchableOpacity
                        style={styles.button}
                        // onPress={}
                    >
                        <Text style={{fontWeight: '900', fontSize: 20}}>SIGNUP</Text>
                    </TouchableOpacity>
                    
                    <View style={styles.signUp}>
                        <Text style={{color: "#fff"}}>Already have account?</Text>
                        <TouchableOpacity
                        onPress ={() => props.navigation.navigate('Login')}
                        title='LOGIN'
                        >
                            <Text  style={{color: "#fff", fontWeight: 'bold', marginLeft: 5}}>SignIn</Text>
                        </TouchableOpacity>
                    </View>
            </View>     
    )
}

export default SignUp;

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        // flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#000",
        height: (consHeigth)
    },
    profilePicture:{
        borderRadius: 50,
        // borderWidth: 2,
        borderColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileImg:{
        borderRadius: 50,
        maxWidth: 100,
        maxHeight: 100
    },
    textInputView:{
        justifyContent: "center",
        alignContent: 'center',
        borderBottomWidth: 0.5,
        borderColor: "#fff",
        color: '#fff',
        width: (consWidth)
    },
    textInput:{
        color: '#fff'
    },
    button:{
        alignItems: 'center',
        backgroundColor:'#DDD',
        borderRadius: 25,
        marginVertical: 30,
        padding: 10,
        width: '50%',
    },
    signUp:{
        display: "flex",
        flexDirection: 'row',
        marginTop: 10,
    }
})