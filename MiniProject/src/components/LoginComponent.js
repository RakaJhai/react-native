import React, {useState} from 'react';
import { View, Image, TextInput, TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import LoginSosmed from './LoginSosmed'; 

const consWidth = Dimensions.get('window').width * 0.8;
const consHeigth = Dimensions.get('window').height;

export default function LoginComponent(props){
    const[userName, setUsername] = useState(null);
    const[password, setPassword] = useState(null);
    
    return(
        // <View>
            <View style={styles.container}>
                    <View style={styles.logoView}>
                        <Image style={styles.logoImg} source={require("../../assets/images/movieReviewLogo.png")}></Image>
                        <Text style={{color: '#fff', fontSize: 30, marginBottom: 30, fontWeight: '900', fontFamily:'monospace'}}>Movie Review</Text>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Enter username"
                            placeholderTextColor= "#fff"
                            onChangeText = {(text) => setUsername(text)}
                            value={userName}
                        ></TextInput>
                    </View>
                
                    <View style={styles.textInputView}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Enter password"
                            placeholderTextColor= "#fff"
                            onChangeText = {(text) => setPassword(text)}
                            value={password}
                        ></TextInput>
                    </View>
                    <View style={{color: '#fff', marginTop: 15}}>
                        <TouchableOpacity>
                            <Text style={{color: '#fff'}}>Forgot your password?</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => props.navigation.navigate('Review')}
                        title="Review guys"
                    >
                        <Text style={{fontWeight: '900', fontSize: 20}}>LOGIN</Text>
                    </TouchableOpacity>
                    
                    <View style={{flexDirection: 'row', width: (consWidth), alignItems: 'center'}}>
                        <View style={{flex: 1, height: 0.5, backgroundColor: '#fff'}} />
                            <Text style={{width: 40, fontSize: 20, textAlign: 'center', color: "#fff"}}>or</Text>
                        <View style={{flex: 1, height: 0.5, backgroundColor: '#fff'}} />
                    </View>
                    <View style={styles.signUp}>
                        <Text style={{color: "#fff"}}>Don't have account?</Text>
                        <TouchableOpacity
                           onPress ={() => props.navigation.navigate('SignUp')}
                           title="REGISTER"
                        >
                            <Text  style={{color: "#fff", fontWeight: 'bold', marginLeft: 5}}>SignUp</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <LoginSosmed />
                    </View>
            </View>
    //   </View> 
       
    )
}

// export default LoginComponent;

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        // flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#000",
        height: (consHeigth)
    },
    logoView:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoImg:{
        maxWidth: 100,
        maxHeight: 100
    },
    textInputView:{
        justifyContent: "center",
        alignContent: 'center',
        borderBottomWidth: 0.5,
        borderColor: "#fff",
        color: '#fff',
        width: (consWidth)
    },
    textInput:{
        color: '#fff'
    },
    button:{
        alignItems: 'center',
        backgroundColor:'#DDD',
        borderRadius: 25,
        marginVertical: 30,
        padding: 10,
        width: '50%',
    },
    signUp:{
        display: "flex",
        flexDirection: 'row',
        marginTop: 10,
    }
})